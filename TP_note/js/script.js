"use strict";

let bonjour = "bonjour";
console.log(bonjour);

function init(){
    console.log("texte");
}

function clique(resultat){
    let nb1 = 24;
    let nb2 = 78;
    resultat = nb1 + nb2;
    console.log(resultat);
}

function changementImage(){
    let image = document.querySelector("#monImage");
    let imageAttribut = image.getAttribute('src');
    console.log(image);
    console.log(imageAttribut);
    
    if(imageAttribut == "img/itadoriSukuna.jpg"){
        image.src = "img/killua.jpeg";
    }
    if(imageAttribut == "img/killua.jpeg"){
        image.src = "img/itadoriSukuna.jpg";
    }
};

let eventChangementImage = document.querySelector("#test");
eventChangementImage.addEventListener("click", changementImage);

function changementCheckbox(){
    let checkImage = document.querySelector("#changeImage");
    let image = document.querySelector("#imageChange");
    let checkTexte = document.querySelector("#changeTexte");
    let texte = document.querySelector("#paragraphe");
    let checkStyle = document.querySelector("#changeStyle");

    if(checkImage.checked == false){
        image.src = "img/killua.jpeg";
    }else{
        image.src = "img/itadoriSukuna.jpg";
    }

    if(checkTexte.checked == true){
        texte.innerHTML = "Naruto (ナルト?) est un shōnen manga écrit et dessiné par Masashi Kishimoto. Naruto a été prépublié dans l'hebdomadaire Weekly Shōnen Jump de l'éditeur Shūeisha entre septembre 1999 et novembre 2014. La série a été compilée en 72 tomes. La version française du manga est publiée par Kana entre mars 2002 et novembre 2016.";
    }else{
        texte.innerHTML = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam placeat, voluptates quas sed ratione minus vero modi repellat expedita natus error maxime aut fugit molestiae temporibus tempore est quo deserunt?";
    }

    if(checkStyle.checked == true){
        texte.classList.add('italic');
    }else{
        texte.classList.remove('italic');
    }
}

let eventChangeImageCheckbox = document.querySelector("#changeImage")
eventChangeImageCheckbox.addEventListener("click", changementCheckbox);

let eventChangeTexteCheckbox = document.querySelector("#changeTexte")
eventChangeTexteCheckbox.addEventListener("click", changementCheckbox);

let eventChangeStyleCheckbox = document.querySelector("#changeStyle")
eventChangeStyleCheckbox.addEventListener("click", changementCheckbox);

// ------------------------------------------------------------------

let changePrenom = false;
function changerPrenom(){
    let prenom = document.querySelector("#bonjourBouton");
    console.log(prenom);
    let bouton = document.querySelector("#bonjourSpan");
    console.log(bouton);
    if(changePrenom === false){
        changePrenom = true;
        bouton.innerHTML = bouton.innerHTML + " " + prenom.innerHTML;
    }else{
        changePrenom = false;
        bouton.innerHTML = "Bonjour"; 
    }
}

let bouton = document.querySelector("#bonjourBouton");
bouton.addEventListener("click", changerPrenom);

// -----------------------------------------------------------------

function champDeSaisi(){
    let input = document.querySelector("#champ");
    let inputValue = input.value;
    let inputLower = inputValue.toLowerCase();
    let texteChamp = document.querySelector("#texteChamp");
    if(!inputLower.includes("cou")){
        return texteChamp.innerHTML += inputValue;
    }
}

function deleteChampSaisi(){
    let texte = document.querySelector("#texteChamp");
    if(texte.innerHTML != null){
        return texte.innerHTML = null;
    }
}

let champBouton = document.querySelector("#champBouton");
champBouton.addEventListener("click", champDeSaisi);

let deleteChampBouton = document.querySelector("#deleteChampBouton");
deleteChampBouton.addEventListener("click", deleteChampSaisi);