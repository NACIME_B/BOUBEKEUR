"use strict";
const bouton1 = document.querySelector("#realisation1");
const bouton2 = document.querySelector("#realisation2");
const bouton3 = document.querySelector("#realisation3");
const boutonFinal = document.querySelector("#boutonFinal");
const titre = document.querySelector("#titreRealisation");
const image = document.querySelector("#imageRealisation");
const paragraphe = document.querySelector("#paragrapheRealisation");


function changeRealisation1(){
    titre.innerText = "Killua";
    image.src = "img/killua-zoldyck.jpg";
    paragraphe.innerHTML = "Kirua Zoldik (キルア=ゾルディック, Kirua Zorudikku) est l'un des quatres protagonistes et le meilleur ami de Gon Freecss dans la série Hunter x Hunter. Il est le troisième enfant de la Famille Zoldik.";
}

function changeRealisation2(){
    titre.innerText = "Nagi";
    image.src = "img/nagi.png";
    paragraphe.innerHTML = "“C’est relou…” est la sempiternelle rengaine de Seishirô Nagi, élève de première qui se complait dans la mollesse. Sa vie connaît un tournant radical quand il fait la rencontre de Reo Mikage. Beau, intelligent, sportif et héritier d’une imposante entreprise, Reo a tout pour lui, mais n’a qu’une seule envie : devenir champion du monde de football.";
    
}

function changeRealisation3(){
    titre.innerText = "Lies of P";
    image.src = "img/lies-of-p.jpg";
    paragraphe.innerHTML = "Lies of P est un jeu d'action Souls-like vous plongeant dans un univers sombre inspiré par le conte de Pinocchio. Vous y incarnez une poupée mécanique évoluant dans la ville morbide et inhumaine de Krat, qui devra mentir pour devenir un petit garçon. Vos choix et vos mensonges auront une influence sur votre histoire et la fin du jeu.";

}

bouton1.addEventListener("click", changeRealisation1);
bouton2.addEventListener("click", changeRealisation2);
bouton3.addEventListener("click", changeRealisation3);

// ---------- Commentaires ---------------

function envoiCommentaire(){
    let nom = document.querySelector("#titreCommentaire");
    let paragraphe = document.querySelector("#paragrapheCommentaire");
    let erreur = document.querySelector("#erreur");
    let erreur2 = document.querySelector("#erreur2");
    let inputNom = document.querySelector("#inputNom");
    let inputNomValue = inputNom.value;
    let inputCommentaire = document.querySelector("#inputCommentaire");
    let inputComValue = inputCommentaire.value;
    if(!inputNomValue.includes(" ")){
        erreur.innerHTML = null;
        nom.innerHTML = inputNomValue;
    }else{
        nom.innerHTML = null;
        console.log("Un nom sans espace est requis");
        erreur.innerHTML = "Pas d'espaces !";
    }

    if(!inputComValue.toLowerCase().includes("papaye")){
        erreur2 = null;
        paragraphe.innerHTML = inputComValue;
    }else{
        paragraphe.innerHTML = null;
        erreur2.innerHTML = "Le mot papaye est INTERDIT";
    }
}

boutonFinal.addEventListener("click", envoiCommentaire);